import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator
import matplotlib.pyplot as plt
import solver, data_processing, writer, data_processing
from matplotlib.backends.backend_pdf import PdfPages

labels = ['N', 'bc', 'L', 'lambda']
G = data_processing.global_options

# each function must take a list of solutions as its argument, which will be passed from main.py after the simulations are complete
def function1(sols):
    print "hello!"
    # print each solution's identifier and its eigenvalues....
    for s in sols:
        print s['ident']
        for x in s['eigenvalues']:
            print x.real
        print '____'

    # sort them then print again
    for s in sols:
        evals = list(s['eigenvalues'])
        evals = sorted(evals)
        print s['ident']
        for x in s['eigenvalues']:
            print x.real
        print '____'

def plot_eigenvalues(sols):
    input_file = G['file_name']
    data_dir = os.path.abspath(input_file + '_dir')
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    plot_dir = os.path.abspath(data_dir + '/plots')
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)
    pp = PdfPages(os.path.abspath(plot_dir + '/eigenvalues.pdf'))

    plt.axes().set_aspect('equal', 'datalim')
    for sol in sols:
        xdata = []
        ydata = []
        for e in sol['eigenvalues']:
            xdata.append(e.real)
            ydata.append(e.imag)

        plt.plot(xdata, ydata, linestyle='', marker='o', ms=3)

        labeltext = ''
        for l in labels:
            if l in sol:
                #labeltext += writer.write_name(l, l, p1=len(l), justify_left=True)
                labeltext += l
                labeltext += ' = ' + writer.write_any(sol[l], l, justify_left=True) + '\n'
        if labeltext != '':
            plt.text(0.1, 0.9, labeltext, ha='left', va='top', transform=plt.axes().transAxes)
        pp.savefig()
        #plt.savefig(pp, format='pdf')
        if len(sols) == 1: plt.show()
        plt.clf()
    pp.close()

def print_eigenvectors(sols):
    output = ''
    for sol in sols:
        if 'eigenvectors' not in sol:
            print "Eigenvectors not found - were they computed?"
            return

        neigs = len(sol['eigenvalues'])

        # here we construct a line to identify which system the eigenvectors are from
        header = ''
        for key, val in sol.iteritems():
            # writer.INDEX_KEYS is just a list of keys which can be used to index different systems i.e. L, N, lambda etc.
            if key in writer.INDEX_KEYS:
                header += key + ' = ' + str(val) + ', '
        output += header + '\n'

        for evec_index in range(neigs):
            output += 'E = ' + str(sol['eigenvalues'][evec_index]) + '\n'
            for evec in sol['eigenvectors']:
                output += str(evec[evec_index]) + '\n'
            output += '\n'

        output += '________\n'
    f = open(G['file_name'] + '.eigenvectors', 'w+')
    f.write(output)





