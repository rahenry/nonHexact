import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator
import matplotlib.pyplot as plt
import solver, data_processing, writer, measure

def omega(j, N):
    return numpy.exp(2.*math.pi*1.J*j/N)

beta1 = omega(1, 3)
beta2 = 1.

systems = [(3,3), (4,2)]
bc = 1
gamma = 1.

def m_element_P(i, j, s, bc, sys):
    N = sys[0]
    L = sys[1]
    d = (j - i)
    if d == -1: return 1.
    if bc == 1: 
        d = d % s
    if d == s-1:
        if bc == 0:
            return 0
        if j == s-1: return beta1
    if d == N-1: 
        z = gamma
        if j == 0: z *= beta2
        if j % N == 0: return z
        if j % N == N-1: return z
        #if j % N == 1 or i % N == 1: return gamma
        return 0
    return 0

def get_parafermions(sys):
    N = sys[0]
    L = sys[1]
    s = N*L
    R = range(0, s)
    M_P = numpy.zeros((s,s))
    for i in R:
        for j in R:
            M_P[i, j] = m_element_P(i, j, s, bc, sys)
    eigs = numpy.linalg.eigvals(M_P)
    return eigs

pfs = []
for sys in systems:
    eigs = get_parafermions(sys)
    for e in eigs:
        if abs(e.imag) < 1E-5 and e.real > 0:
            pfs.append((sys[0], e.real))


sols = [0.]
for pf in pfs:
    new_sols = []
    for s in sols:
        for x in range(pf[0]):
            new_sols.append(s + pf[1] * omega(x, pf[0]))
    sols = new_sols

xdata, ydata = [], []
for s in sols:
    xdata.append(s.real)
    ydata.append(s.imag)

plt.plot(xdata, ydata, linestyle='', marker='o', ms=3)
plt.axes().set_aspect('equal', 'datalim')
plt.show()
