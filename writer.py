import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator
import matplotlib.pyplot as plt
import solver, data_processing
from decimal import Decimal

INDEX_KEYS = ['bc', 'N', 'L', 'lambda', 'index', 'model', 'TOLERANCE']
FLOAT_PRECISION_DEFAULT = 16
FLOAT_WIDTH_DEFAULT = 26
INTEGER_WIDTH_DEFAULT = 6
PRINT_COMPLEX = 1
OUTPUT_SETTINGS= {
    'lambda' : {'p1' : 8, 'p2' : 2},
    'bc' : {'values' : {0 : 'open', 1 : 'periodic', 2 : 'twisted'},
        'p1' : 9},
    'N' : {'p1' : 3},
    'L' : {'p1' : 4},
    'index' : {'p1' : 4},
    'model' : {'p1' : 5},
    'eigenvectors' : None,
    'TOLERANCE' : {'sort_style' : 'reverse', 'style':'scientific', 'p1' : 11},
    'tol' : {'sort_style' : 'reverse', 'style':'scientific', 'p1' : 11},
    'run_time' : {'p1' : 10, 'p2' : 5},
    }
G = data_processing.global_options

def make_sci(x):
    res = '{:.0E}'.format(Decimal(x))
    return res
#"{:.2E}".format(Decimal('40800000000.00000000000000'))
    
def write_any(x, name, p1=None, p2=None, justify_left=False):
    if data_processing.is_iterable(x): x = x[0]
    if name in OUTPUT_SETTINGS:
        ost = OUTPUT_SETTINGS[name]
        if 'values' in ost:
            if x in ost['values']: x = ost['values'][x]
        if 'p1' in ost: p1 = ost['p1']
        if 'p2' in ost: p2 = ost['p2']
        if 'style' in ost:
            if ost['style'] == 'scientific':
                x = make_sci(x)

    dtype = 'i'
    just = ''
    if isinstance(x, complex):
        if 'imag' in name.lower():
            x = x.imag
        else:
            x = x.real
    if isinstance(x, int):
        if not p1: p1 = INTEGER_WIDTH_DEFAULT
        dtype = 'i'
    if isinstance(x, float):
        if not p1: p1 = FLOAT_WIDTH_DEFAULT
        if not p2: p2 = FLOAT_PRECISION_DEFAULT
        dtype = 'f'
    if not p1: p1 = FLOAT_WIDTH_DEFAULT
    if isinstance(x, str):
        dtype = 's'
        just = '+'
    if justify_left: just = '-'
    z = '%' + just + str(p1) + dtype
    if isinstance(x, float):
        z = '%' + just + str(p1) + '.' + str(p2) + dtype
    return z % x

def write_name(x, n, p1=None, justify_left=False):
    if n in OUTPUT_SETTINGS:
        ost = OUTPUT_SETTINGS[n]
        if 'values' in ost:
            if x in ost['values']: x = ost['values'][x]
        if 'p1' in ost: p1 = ost['p1']
        if 'p2' in ost: p2 = ost['p2']
    if data_processing.is_iterable(x): x = x[0]
    if isinstance(x, int):
        if not p1: p1 = INTEGER_WIDTH_DEFAULT
    if isinstance(x, float):
        if not p1: p1 = FLOAT_WIDTH_DEFAULT
    if not p1: p1 = FLOAT_WIDTH_DEFAULT
    z = '%'
    if justify_left: z += '-'
    z = z + str(p1) + 's'
    return z % n

def generate_output(s, scheme):
    res = ''
    for x in scheme:
        if not data_processing.is_iterable(x): x = [x]
        try:
            res += write_any(s[x[0]], x[0], *x[1:])
        except KeyError:
            pass
    return res

def generate_header(s, scheme):
    res = ''
    for x in scheme:
        if not data_processing.is_iterable(x): x = [x]
        try:
            res += write_name(s[x[0]], x[0], *x[1:])
        except KeyError:
            pass
    return res

def reverse_sort_helper(x):
    if x == 0: return 1E100
    else: return 1./x

def sort_sols(sol_list, scheme):
    if len(sol_list) == 0:
        print "You have no solutions!"
        return
    for x in scheme:
        if x in sol_list[0]:
            try:
                z = lambda y: y[x]
                if x in OUTPUT_SETTINGS:
                    q = OUTPUT_SETTINGS[x]
                    if 'sort_style' in q:
                        if q['sort_style'] == 'reverse':
                            z = lambda y: reverse_sort_helper(y[x])
                #sol_list = sorted(sol_list, key=lambda y: y[x])
                sol_list = sorted(sol_list, key=z)
            except TypeError:
                pass
            except KeyError:
                pass
                
    return sol_list

def make_ident_name(c):
    keys = INDEX_KEYS
    res = ''
    for k in keys:
        if k in c:
            res += k + ' ' + str(c[k])[:6] + ', '
    return res
    
def make_ident(c):
    keys = INDEX_KEYS
    res = []
    for k in keys: 
        if k in c:
            try:
                res.append(float(c[k]))
            except ValueError:
                res.append(float(hash(c[k]) % 10000))
    return tuple(res)

