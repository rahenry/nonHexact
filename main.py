import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator
import matplotlib.pyplot as plt
import solver, data_processing, writer, measure


data = data_processing.read_inputs()
G = data_processing.global_options

header = ''
sol_list = []
for input_file, d in data.iteritems():
    for ident, s in d.iteritems():
        if header == '': header = writer.generate_header(s, G['output_scheme'])
        s['output'] = writer.generate_output(s, G['output_scheme'])
        sol_list.append(s)

#plotting.plot_eigenvalues(data)


sol_list = writer.sort_sols(sol_list, G['sorting_scheme'])

output = header
for s in sol_list:
    output += '\n' + s['output']
output += '\n'

output_file = sys.argv[1] + '_output'
f = open(output_file, 'w+')
f.write(output)
f.close()

import analysis
if isinstance(G['analysis'], str):
    G['analysis'] = [G['analysis']]
for f in G['analysis']:
    exec('analysis.' + f + '(sol_list)')

#latex.table1(sol_list)
import latex2
latex2.simple_output(sol_list)

lambdas = []
for s in sol_list:
    l = s['lambda']
    if l not in lambdas: lambdas.append(l)

lambdas = map(lambda x: 1./x, lambdas)
lambdas = sorted(lambdas)
zzz = ''
for l in lambdas:
    zzz += '$\\lambda = ' + str(l) + '$ & '

