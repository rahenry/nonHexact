import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, os, shutil
import matplotlib.pyplot as plt
import writer, data_processing
import timeit
from scipy.sparse.linalg import ArpackNoConvergence
from scipy.optimize import curve_fit


SIGMA_OFFSET = 0 

def sigma(i, j, N):
    return (i / (N ** j)) % N

def normalised(s):
    norm = numpy.linalg.norm(s)
    if norm < 1E-12: return s
    return s / norm

def index(initial, j_modified, sigma_j_f, N):
    sigma_j_i = sigma(initial, j_modified, N)
    return initial + ((N ** j_modified)  * (sigma_j_f - sigma_j_i))
    
def cZ_expected(sol):
    gamma = sol['lambda']
    p = -2.
    if sol['bc'] == 1: p = -4.
    if gamma <= 1: return 0.

    else: return (1. - (gamma ** -p))

def cX_expected(sol):
    gamma = sol['lambda']
    if gamma > 1.: return 0.
    return pow(1. - gamma*gamma, 0.25)

def calc_charge_mapping(sol, i):
    res = 0
    L = sol['L']
    N = sol['N']
    for j in range(L):
        sigma_j = sigma(i, j, sol['N'])
        z = (sigma_j - 1) % N
        res += (N**j) * z
    return res

def sort_eigs(e):
    idx = e[0].argsort()[::-1]   
    idx = e[0].argsort()
    return (e[0][idx], e[1][:,idx])

def calc_correlations_new(sol, j1, j2):
    j1 = j1-1
    j2 = j2-1
    default = (0,0,0)
    if not sol['e']: return default
    G = data_processing.global_options
    if not G['calc_correlations']:
        return default

    N = sol['N']
    L = sol['L']
    M = sol['N'] ** sol['L']
    omega = cmath.exp(2.0 * math.pi * 1.0J / sol['N'])
    Q = range(M) # indices of all states
    Z_mapping = sol['Z_mapping']
    X_mapping = sol['X_mapping']
    X_conj_mapping = sol['X_conj_mapping']
    X_conj_mapping = sol['X_mapping']

    bra0 = numpy.conj(sol['eigenvectors'][0])
    bra0 = normalised(bra0)
    ket0 = normalised(sol['eigenvectors'][0])

    cX, cZ, cZ1 = 0,0,0
    for i in Q:
        cZ += bra0[i] * pow(omega, sigma(i, j1, N)) * pow(omega, -sigma(i, j2, N)) * ket0[i]
        cZ1 += bra0[i] * pow(omega, sigma(i, j1, N)) * ket0[i]

    ket_cX = numpy.zeros_like(ket0)
    ket_temp = numpy.zeros_like(ket0)
    for i in Q:
        ket_temp[X_conj_mapping[j2][i]] += ket0[i]
    for i in Q:
        ket_cX[X_mapping[j1][i]] += ket_temp[i]
    for i in Q:
        cX += bra0[i] * ket_cX[i]

    return cX, cZ, cZ1
    
def calc_correlations(sol):
    if not sol['e']: return
    G = data_processing.global_options
    if not G['calc_correlations']:
        return
    surface_state = 1
    min_energy = 0.0
    for i, x in enumerate(sol['eigenvalues']):
        if i == 0: continue
        if x.real < min_energy and x.imag < 0:
        #if x.real < min_energy:
            surface_state = i
            min_energy = x.real
        
    N = sol['N']
    L = sol['L']
    M = sol['N'] ** sol['L']
    omega = cmath.exp(2.0 * math.pi * 1.0J / sol['N'])
    Q = range(M) # indices of all states
    Z_mapping = sol['Z_mapping']
    X_mapping = sol['X_mapping']
    X_conj_mapping = sol['X_conj_mapping']
    #X_conj_mapping = sol['X_mapping']

    bra = numpy.conj(sol['eigenvectors'][0])
    bra = normalised(bra)
    bra0 = bra
    ket0 = normalised(sol['eigenvectors'][0])
    ket1 = normalised(sol['eigenvectors'][surface_state])
    bra1 = normalised(numpy.conj(sol['eigenvectors'][surface_state]))
    #bra1 = numpy.conj(sol['eigenvectors'][surface_state])

    cZ = 0
    cZ12 = 0
    cZ_bulk = 0
    i_bulk = L/2
    cZ_conj1 = 0
    cZ_conjL = 0
    surfaceZ = 0
    for i in Q:
        cZ += bra[i] * pow(omega, sigma(i,0, N)) * pow(omega, sigma(i, L-1, N)) * ket0[i]
        cZ12 += bra[i] * pow(omega, sigma(i,0, N)) * pow(omega, sigma(i, 1, N)) * ket0[i]
        cZ_bulk += bra[i] * pow(omega, sigma(i,i_bulk, N)) * pow(omega, sigma(i, i_bulk+1, N)) * ket0[i]
        cZ_conj1 += bra[i] * pow(omega, -sigma(i,0, N)) * pow(omega, sigma(i, L-1, N)) * ket0[i]
        cZ_conjL += bra[i] * pow(omega, sigma(i,0, N)) * pow(omega, -sigma(i, L-1, N)) * ket0[i]
        surfaceZ += bra1[i] * pow(omega, sigma(i,0, N)) * ket0[i]
        #surfaceZ += bra0[i] * pow(omega, sigma(i,0, N)) * ket1[i]

    cX = 0
    cX12 = 0
    cX_bulk = 0
    cX_conj1 = 0
    cX_conjL = 0
    ket_temp = numpy.zeros_like(ket0)
    ket_temp_bulk = numpy.zeros_like(ket0)
    ket_temp_conj1 = numpy.zeros_like(ket0)
    for i in Q:
        ket_temp[X_mapping[0][i]] += ket0[i]
        ket_temp_bulk[X_mapping[i_bulk][i]] += ket0[i]
        ket_temp_conj1[X_conj_mapping[0][i]] += ket0[i]
    ket_cX = numpy.zeros_like(ket0)
    ket_cX12 = numpy.zeros_like(ket0)
    ket_cX_bulk = numpy.zeros_like(ket0)
    ket_cX_conj1 = numpy.zeros_like(ket0)
    ket_cX_conjL = numpy.zeros_like(ket0)
    for i in Q:
        ket_cX[X_mapping[L-1][i]] += ket_temp[i]
        ket_cX12[X_mapping[1][i]] += ket_temp[i]
        ket_cX_bulk[X_mapping[i_bulk+1][i]] += ket_temp_bulk[i]
        ket_cX_conj1[X_mapping[L-1][i]] += ket_temp_conj1[i]
        ket_cX_conjL[X_conj_mapping[L-1][i]] += ket_temp[i]
    for i in Q:
        cX += bra[i] * ket_cX[i]
        cX12 += bra[i] * ket_cX12[i]
        cX_bulk += bra[i] * ket_cX_bulk[i]
        cX_conj1 += bra[i] * ket_cX_conj1[i]
        cX_conjL += bra[i] * ket_cX_conjL[i]

    surfaceX = 0
    ket_sX = numpy.zeros_like(ket0)
    for i in Q:
        ket_sX[X_mapping[0][i]] += ket0[i]
    for i in Q:
        surfaceX += bra1[i] * ket_sX[i]

    sol['cZ'] = cZ
    sol['cX'] = cX
    sol['cZ_conj1'] = cZ_conj1
    sol['cZ_conjL'] = cZ_conjL
    sol['cX_conj1'] = cX_conj1
    sol['cX_conjL'] = cX_conjL
    sol['surfaceX'] = abs(surfaceZ)
    sol['surfaceZ'] = abs(surfaceX)
    sol['cZ_bulk'] = cZ_bulk
    sol['cX_bulk'] = cX_bulk


def generate_mappings(sol):
    N = sol['N']
    L = sol['L']
    bc = sol['bc']
    gamma = sol['lambda']

    omega = cmath.exp(2.0 * math.pi * 1.0J / N)
    M = N**L # size of hilbert space
    Q = range(M) # indices of all states
    s0 = []
    X_mapping = []
    X_conj_mapping = []
    Z_mapping = []
    charge_mapping = []

    for j in range(L):
        X_mapping.append([])
        X_conj_mapping.append([])
    for i in range(N ** L):
        if (bc == 3):
            charge_mapping.append(calc_charge_mapping(sol, i))
        s0.append(1.0)
        for j in range(L):
            sigma_j = sigma(i, j, N)
            if (sigma_j < 1):
                X_mapping[j].append(index(i, j, sigma_j+1, N))
                X_conj_mapping[j].append(index(i, j, N-1, N))
            elif (sigma_j < N - 1):
                X_mapping[j].append(index(i, j, sigma_j+1, N))
                X_conj_mapping[j].append(index(i, j, sigma_j-1, N))
            else:
                X_mapping[j].append(index(i, j, 0, N))
                X_conj_mapping[j].append(index(i, j, sigma_j-1, N))


        Z_coef = 0.
        R = [1]
        if sol['model'] == 'SICP':
            R = range(1, N)
        for r in R:
            u = 1.
            if sol['model'] == 'SICP':
                u = SICP_factor(r, N)

            for j in range(L-1):
                Z_coef += u * pow((omega ** (sigma(i, j, N) -SIGMA_OFFSET )) * ((omega ** (sigma(i, j+1, N) - SIGMA_OFFSET)) ** (N-1)), r)

            if (bc == 1 or bc == 2):
                j = L-1
                g = 1.0
                if bc == 2: g = omega
                Z_coef += g * u * pow((omega ** (sigma(i, j, N) - SIGMA_OFFSET)) * ((omega ** (sigma(i, 0, N) -SIGMA_OFFSET)) ** (N-1)), r)

        Z_mapping.append(Z_coef)
    sol['X_mapping'] = X_mapping
    sol['Z_mapping'] = Z_mapping
    sol['X_conj_mapping'] = X_conj_mapping
    return (X_mapping, X_conj_mapping, Z_mapping, charge_mapping)

def SICP_factor(r, N):
    u = numpy.exp(math.pi * 1.J * (2.*r-N)/2./N)/numpy.sin(math.pi*r/N)
    return u

def eigs_in_time(m, neigs, tol, time_limit):
    iters = 3
    iter_mult = 3.3333
    while (True):
        start_time = timeit.default_timer()
        try:
            e = scipy.sparse.linalg.eigs(m, k=neigs, maxiter=iters, tol=tol, which='SR')
            end_time = timeit.default_timer()
            t = end_time - start_time
            #print tol, iters, t
            return (e, time_limit > t)
        except ArpackNoConvergence as ex:
            iters *= iter_mult
        end_time = timeit.default_timer()
        t = end_time - start_time
        #print tol, iters, t, time_limit
        if t > time_limit:
            return (None, False)

        
def sparse_eigs(sol, m, neigs, tol):
    G = data_processing.global_options
    time_limit = G['time_limit']
    tol_test = sol['TOLERANCE'] * 1000
    tol_mult = 5.0
    res = None
    sol['has_solution'] = False

    while (True):
        z = eigs_in_time(m, neigs, tol_test, time_limit)
        if z[0]:
            res = z[0]
            if tol_test < tol:
                sol['conv'] = True
                sol['tol'] = tol_test
                sol['has_solution'] = True
                return res
            tol_test /= tol_mult
            if not z[1]:
                sol['conv'] = False
                sol['tol'] = tol_test
                sol['has_solution'] = True
                return res
        else:
            sol['conv'] = False
            sol['tol'] = tol_test
            return res

def examine_boundary_effects(sol):
    L = sol['L']
    xdata = range(L-1)
    ydata, ydata_R, ydata_I, ydata_arg = [], [], [], []
    for x in range(L-1):
        a = calc_correlations_new(sol, x+1, x+2)
        b = calc_correlations_new(sol, int(L/2), int(L/2+1))
        bzz, bxx, bz = a[0]-b[0], a[1]-b[1], a[2]-b[2]
        #bzz, bxx, bz = a[0], a[1], a[2]
        ydata.append((bzz, bxx, bz))
        ydata_R.append((bzz.real, bxx.real, bz.real))
        ydata_I.append((bzz.imag, bxx.imag, bz.imag))
        ydata_arg.append((numpy.angle(bzz), numpy.angle(bxx), numpy.angle(bz)))

    directory = 'boundary'
    if not os.path.exists(directory):
        os.mkdir(directory)

    labels = ['_R', '_I', '_arg']
    label_index = 0

    for q in range(3):
        y = [i[q] for i in ydata]
        print q, numpy.sum(y)
    for Y in [ydata_R, ydata_I, ydata_arg]:
        for q in range(2):
        #for q in [2]:
            y = [i[q] for i in Y]
            plt.scatter(xdata, y)
        name = 'boundary_' +str(L) + '_' + str(sol['gamma'])+labels[label_index]
        label_index += 1
        plt.savefig(os.path.join(directory, name)+'.png')
        plt.clf()

    def exp_fun(x, a, b, c):
        return a*numpy.exp(-b*x) + c
    def cosh_fun(x, a, b, c):
        xlim = int(L/2. - 1)
        return a*numpy.cosh(b*(x-xlim)) + c
    def sinh_fun(x, a, b, c):
        return -a*numpy.sinh(b*(x-L/2)) + c

    def fit(X, Y, fun):
        #return curve_fit(fun, X[:L/2], Y[:L/2], p0=(1, 1e-6, 1))[0]
        return curve_fit(fun, X, Y, p0=(1, 1, 0))[0]
        
    return
    X = numpy.array(xdata)
    for q in range(L):
        Y_R = [i[q] for i in ydata_R]
        #fit_R = fit(X, Y_R, cosh_fun)
        xlim = int(L/2. - 1)
        print Y_R, Y_R[:xlim]
        fit_R = fit(X[:xlim], Y_R[:xlim], exp_fun)
        #Y_I = [i[q] for i in ydata_I]
        #fit_I = fit(X, Y_I, sinh_fun)
        #print fit_R, fit_I
        for x in range(L-1):
            #print cosh_fun(x, *fit_R), Y_R[x]
            print exp_fun(x, *fit_R), Y_R[x]
        continue
        print '...'
        xlim = L
        fit_R = fit(X[:xlim], Y_R[:xlim], cosh_fun)
        for x in range(L-1):
            print cosh_fun(x, *fit_R), Y_R[x]

 
def solve(sol):
    start_time = timeit.default_timer()
    G = data_processing.global_options
    N = sol['N']
    L = sol['L']
    gamma = sol['lambda']
    bc = sol['bc']
    TOLERANCE = sol['TOLERANCE']
    omega = cmath.exp(2.0 * math.pi * 1.0J / N)
    M = N**L # size of hilbert space
    Q = range(M) # indices of all states

    ident = writer.make_ident(sol)

    X_mapping, X_conj_mapping, Z_mapping, charge_mapping = generate_mappings(sol)

    s0 = []
    rows = []
    cols = []
    data = []
    count = 0

    R = range(1, N)
    if (sol['model'] == 'SICP'):
        for i in Q:
            rows.append(i)
            cols.append(i)
            x = 0.
            x -= gamma * (Z_mapping[i])
            data.append(x)

            for j in range(L):
                for r in R:
                    k = i
                    z = 0
                    while (z < r):
                        k = X_mapping[j][k]
                        z += 1

                    Z = 0
                    K = i
                    while (Z < r+5):
                        K = X_mapping[j][K]
                        Z += 1
                    rows.append(i)
                    cols.append(k)
                    data.append(-SICP_factor(r, N))

    else:
        for i in Q:
            rows.append(i)
            cols.append(i)
            data.append(-gamma * Z_mapping[i])

            for x in range(L):
                j = X_mapping[x][i]
                rows.append(i)
                cols.append(j)
                data.append(-1)
            if sol['bc'] == 3:
                rows.append(i)
                j = charge_mapping[i]
                cols.append(j)
                g = (omega ** (sigma(i, L-1, N) - SIGMA_OFFSET)) * ((omega ** (sigma(i, 0, N) -SIGMA_OFFSET)) ** (N-1))
                g = (omega ** (sigma(i, 0, N) - SIGMA_OFFSET)) * ((omega ** (sigma(i, L-1, N) -SIGMA_OFFSET)) ** (N-1))
                a = 1.
                data.append(-gamma * g)

    e = []
    neigs = N**L
    if isinstance(G['n_solutions'], float):
        neigs = int(neigs * G['n_solutions'])

    elif G['n_solutions'] < 0:
        neigs -= G['n_solutions']
    elif G['n_solutions'] > 0:
        neigs = G['n_solutions']


    time = timeit.default_timer()
    print "Starting eigensolver... construction took " + str(time-start_time)
    if sol['eig_method'] == 'smallest_mag':
        m = scipy.sparse.csc_matrix((data, (rows, cols)), (M, M))
        z = N ** L - 2
        if neigs > z: neigs = z
        e = scipy.sparse.linalg.eigs(m, k=neigs, maxiter = 10000000, tol=TOLERANCE, which='LM', sigma=0)
    elif sol['eig_method'] == 'time_limit':
        m = scipy.sparse.csc_matrix((data, (rows, cols)), (M, M))
        e = sparse_eigs(sol, m, neigs, TOLERANCE)

    elif sol['eig_method'] == 'sparse':
        m = scipy.sparse.coo_matrix((data, (rows, cols)), (M, M))
        z = N ** L - 2
        if neigs > z: neigs = z
        e = scipy.sparse.linalg.eigs(m, k=neigs, maxiter = 10000, tol=TOLERANCE, which='SR')
    elif sol['eig_method'] == 'all_dense':
        m = numpy.zeros((M,M), dtype=numpy.complex_)
        neigs = N**L
        for (i,j,d) in zip(rows, cols, data):
            m[i, j] = d
        e = scipy.linalg.eig(m)
    else:
        print "Invalid eig_method!"
        exit()

    try:
        e = sort_eigs(e)
        sol['energy'] = min(e[0])
        sol['eigenvalues'] = e[0]
    except TypeError:
        sol['energy'] = 0
        pass
    sol.update({
            'N' : N,
            'L' : L,
            'gamma' : gamma,
            'bc' : bc,
            #'ident' : ident,
            'e' : sol['energy'] / L,
            'energy_expected' : exact_eigenvalue(sol),
            })

    sol.update({
        'cZ_expected': cZ_expected(sol),
        'cX_expected': cX_expected(sol),
        'e_infinity' : sol['energy_expected'],
        'e_exact' : exact_eig2(sol),
        })
    evecs = []
    if e:
        for ind in range(neigs):
            new_evec = []
            for x in e[1]:
                new_evec.append(x[ind])
            evecs.append(new_evec)

    end_time = timeit.default_timer()
    sol['run_time'] = end_time-start_time
    sol['eigenvectors'] = evecs
    calc_correlations(sol)
    sol['cZ_new'], sol['cX_new'], z = calc_correlations_new(sol, 1, L)
    sol['cZ_bulk'], sol['cX_bulk'], sol['cZ_bulk'] = calc_correlations_new(sol, int(L/2), int(L/2)+1)
    sol['cZ12'], sol['cX12'], sol['cZ_1'] = calc_correlations_new(sol, 1, 2)
    #print sol['cZ_new'], sol['cX_new'], z 
    #print sol['cZ_bulk'], sol['cX_bulk'], sol['cZ_bulk'] 

    examine_boundary_effects(sol)

    del sol['eigenvectors']
    del sol['X_mapping']
    del sol['Z_mapping']
    del sol['X_conj_mapping']
    return sol

def eps(j, L, N, gamma):
    kj = 2.*j*math.pi/(2.*L+1.)
    return pow(1. + gamma**N + 2.*(gamma**(.5*N)) * math.cos(kj), 1./N)


def exact_eig2(sol):
    L = sol['L']
    N = sol['N']
    gamma = sol['gamma']
    res = 0.0
    for j in range(L):
        res -= eps(j+1, L, N, gamma)
    return res / L
def exact_eigenvalue(sol):
    L = sol['L']
    N = sol['N']
    gamma = sol['lambda']
    if False:
    #if gamma == 1.:
        res = 0.0
        for j in range(L):
            res -= eps(j+1, L, N, gamma)
        return res / L

    if sol['model'] == 'SICP':
        res = 0.
        for l in range(1, N):
            res += (1.+gamma) * scipy.special.hyp2f1(-0.5, float(l)/N, 1., 4.*gamma/((1.+gamma)**2.))
        return -res
    else:
        if (gamma >= 1.):
            return -(1+gamma**N)**(1./N) * scipy.special.hyp2f1(-1./2./N, (-1./N+1.)/2., 1., 4.*gamma**N/((1.+gamma**N)**2))
            return -(1.-gamma**N)**(1./N) * scipy.special.hyp2f1(-1./N,1.-1./N,1.,1./(1.-gamma**(-N)))
            return -gamma * scipy.special.hyp2f1(-1./N, -1./N, 1., (1./gamma)**N)

        return -scipy.special.hyp2f1(-1./N, -1./N, 1., gamma**N)

