import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator, re
import matplotlib.pyplot as plt
import solver, data_processing, writer, data_processing
from matplotlib.backends.backend_pdf import PdfPages

key_map = {'lambda' : 'lambda'}
key_map = {'lambda' : 'lambda'}
val_map = {'lambda' : lambda x: 1./x} # oh dear
observables = {'surfaceZ':{'p1':10, 'p2':6, 'type':'float'},
               'surfaceX':{'p1':10, 'p2':6, 'type':'float'},
               'cX':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ_conj1':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ_conjL':{'p1':10, 'p2':6, 'type':'complex'},
               'cX_conj1':{'p1':10, 'p2':6, 'type':'complex'},
               'cX_conjL':{'p1':10, 'p2':6, 'type':'complex'},
               'cX12':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ_1':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ12':{'p1':10, 'p2':6, 'type':'complex'},
               'cX_new':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ_new':{'p1':10, 'p2':6, 'type':'complex'},
               'cX_bulk':{'p1':10, 'p2':6, 'type':'complex'},
               'cZ_bulk':{'p1':10, 'p2':6, 'type':'complex'},
               }

use_observables = {'cX'}
use_observables = {'surfaceZ', 'cX', 'surfaceX', 'cZ', 'cZ_conj1', 'cZ_conjL', 'cX_conj1', 'cX_conjL', 'cX12', 'cZ12', 'cX_new', 'cZ_new', 'cX_bulk', 'cZ_bulk', 'cZ_1'}
autotable_keys = ['cX', 'cX12', 'surfaceX', 'cZ', 'cZ12', 'surfaceZ']
autotable_keys = ['cX', 'cX12', 'surfaceX', 'cZ', 'cZ12', 'surfaceZ']
autotable_keys = ['cX_new', 'cZ_new', 'cX_bulk', 'cZ_bulk', 'cX12', 'cZ12', 'surfaceX', 'surfaceZ', 'cX_1']

def simple_output(sols):
    autotable_entries = []
    for o in observables:
        res = ''
        if o not in use_observables: continue
        z = tabulate2(sols, o)
        if len(z) == 0:
            #print "No data found for " + o
            continue
        autotable_entries += z
        #autotable_entries += [['\\newpage\n\n']]
    a = generate_autotable(autotable_entries)
    f = open('autotable.tex', 'w+')
    f.write(a)
    f.close()

def make_tex_entry(key, N, bc, p):
    res = ''
    res += '\\begin{table}\n'
    res += '\\resizebox{\\textwidth}{!}{\n'
    res += '\\nonHtable{'+key+'}{'+str(N)+'}{'+str(bc)+'}{'+str(p)+'}\n'
    res += '}\n'
    cap = key + ', N = ' + str(N) + ', bc = ' + str(bc) 
    cap = cap.replace('_', '\\_')
    res += '\\caption{' + cap + '}\n'
    res += '\\end{table}\n'
    info = (key, N, bc)
    return (info, res)

def generate_autotable(d):
    res = ''
    res += '\\documentclass[10pt]{article}\n'
    res += '\\usepackage{nonHtable}\n'
    res += '\\begin{document}\n'
    for z in d:
        res += z[-1]
    res += '\\end{document}\n'
    return res

def tabulate2(sols, o):
    res = []
    G = data_processing.global_options

    fs = [(lambda x: x, '')]
    autotable_entries = []
    #if observables[o]['type'] == 'complex':
        #fs = [(lambda x: x.real, 'real'), (lambda x: x.imag, 'imag')]
    for f in fs:
        for N in G['params']['N']:
            for bc in [0, 1]:
                res = ''
                res += make_table2(sols, {'bc':bc, 'N':N}, 'lambda', 'L', o, observables[o]['p1'], observables[o]['p2'], f[0])
                obs_name = make_obs_name(o)
                fname = obs_name + '_N' + str(N) + '_bc' + str(bc)
                if f[1] != '': fname += '_' + f[1]
                fname += '.txt'
                if not os.path.exists('simulation_data'):
                    os.mkdir('simulation_data')
                file = open('simulation_data/' + fname, 'w+')
                file.write(res)
                if obs_name in autotable_keys: autotable_entries.append(make_tex_entry(obs_name, N, bc, 6))
    return autotable_entries

def test_filter(s, filt):
    for f in filt:
        if not f in s: return False
        if not s[f] == filt[f]: return False
    return True

def make_obs_name(o):
    replacers = [('X', 'Z'), ('Z', 'X')]
    res = o
    for r in replacers:
        if r[0] in res:
            res = res.replace(r[0], r[1])
            return res
    return res

def make_text_table_header(Ns, xkey, corner_title):
    res = ''
    #res += corner_title
    #for N in Ns:
        #res += 'x,'
    #res += 'x\n'
    Ns = list(Ns)
    #if xkey in key_map:
        #Ns = map(val_map[xkey], Ns)
        #Ns = sorted(Ns)
    for N in Ns:
        x_temp = N
        xkey_temp = xkey
        if xkey in key_map:
            xkey_temp = key_map[xkey]
            x_temp = val_map[xkey](N)
        #res += q + ' = ' + str(y_temp)
        #res += q
        
        res += ',' + xkey_temp + ' = ' + str(x_temp)
        z = xkey_temp + ' = ' + str(x_temp)
        if 'lambda' in z:
            z = z.replace('lambda', '\\lambda')
            z = '{$' + z  + '$}'
        #res += ',' + z
    res += '\n'
    return res
 
def make_table2(sols, filt, xkey, ykey, entrykey, p1, p2, f):
    xmin = sols[0][xkey]
    xmax = xmin
    ymin = sols[0][ykey]
    ymax = ymin
    sol_index = {}
    xs = set()
    ys = set()
    for s in sols:
        if not test_filter(s, filt): continue
        sol_index[(s[xkey], s[ykey])] = s
        xs.add(s[xkey])
        ys.add(s[ykey])

    xs = sorted(xs)
    ys = sorted(ys)
    #ys.reverse()
    res = ''
    corner_title = 'N = ' + str(filt['N'])
    
    res += make_text_table_header(xs, xkey, corner_title)

    for y in ys:
        q = ykey
        #if q in key_map: q = key_map[q]
        y_temp = y
        #if ykey in key_map: y_temp = val_map[ykey](y)
        #res += q + ' = ' + str(y_temp)
        #res += q
        res += str(y_temp)
        for x in xs:
            if (x, y) in sol_index:
                z = '0'
                try:
                    z = sol_index[(x,y)][entrykey]
                    z = f(z)
                    if isinstance(z, complex):
                        z = z.real
                        z = str(z)
                        #z = z.strip('()')
                        #z = z.replace('j', '')
                        #z = z.replace('+', ' ')
                        #z = re.sub('([0-9])-', '\\1 ', z)
                    else:
                        z = str(z)
                except KeyError:
                    pass
                res += ',' + str(z)
        res += '\n'

    return res
 
