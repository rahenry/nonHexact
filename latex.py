import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os, operator
import matplotlib.pyplot as plt
import solver, data_processing, writer, data_processing
from matplotlib.backends.backend_pdf import PdfPages

key_map = {'lambda' : '\\lambda'}
val_map = {'lambda' : lambda x: 1./x} # oh dear
observables = {'surfaceZ':{'p1':10, 'p2':6, 'type':float},
               'cX':{'p1':10, 'p2':6, 'type':complex},
               'cZ':{'p1':10, 'p2':6, 'type':complex},
               }

use_observables = {'cX'}
use_observables = {'surfaceZ', 'cX', 'cZ'}


def test_filter(s, filt):
    for f in filt:
        if not f in s: return False
        if not s[f] == filt[f]: return False
    return True

    
def table1(sols):
    res = ''
    for o in observables:
        if o not in use_observables: continue
        res += o + '\n\n'
        res += tabulate(sols, o)
        res += '\n'
        res += '\n'

    f = open('nonH_tables.tex', 'w+')
    f.write(res)

def tabulate(sols, observable):
    G = data_processing.global_options
    res = ''
    for N in G['params']['N']:
        res += make_table(sols, {'bc':0, 'N':N}, 'L', 'lambda', observable, observables[observable]['p1'], observables[observable]['p2'])
        res += '\n\n'
    return res

def test_filter(s, filt):
    for f in filt:
        if not f in s: return False
        if not s[f] == filt[f]: return False
    return True

def write_key_eq(key, val):
    if key in val_map:
        val = val_map[key](val)
    if key in key_map:
        key = key_map[key]
    res = '$' + key + ' = ' + str(val) + '$'
    return res

def make_latex_table_header(Ns, xkey, corner_title):
    #res = '\\begin{tabu}{\\textwidth}{|l|'
    res = '\\begin{tabu} to \\textwidth {@{} | l | '
    for N in Ns:
        #res += 'X|'
        res += 'X[c] | ' 
    res += ' @{}}\n'
    res += '\\hline' + '\n'
    #res += '{} '
    res += corner_title + ' '
    for N in Ns:
        #res += ' & ' + xkey + ' = ' + str(N)
        res += ' & ' + write_key_eq(xkey, N)
    res += '\\\\' + '\n'
    res += '\\hline' + '\n'
    return res

def make_table(sols, filt, xkey, ykey, entrykey, p1, p2):
    xmin = sols[0][xkey]
    xmax = xmin
    ymin = sols[0][ykey]
    ymax = ymin
    sol_index = {}
    xs = set()
    ys = set()
    for s in sols:
        if not test_filter(s, filt): continue
        sol_index[(s[xkey], s[ykey])] = s
        xs.add(s[xkey])
        ys.add(s[ykey])

    xs = sorted(xs)
    ys = sorted(ys)
    ys.reverse()
    res = ''
    corner_title = '$N = ' + str(filt['N']) + '$ '
    res += make_latex_table_header(xs, xkey, corner_title)

    for y in ys:
        #res += ykey + ' = ' + str(y)
        res += write_key_eq(ykey, y)
        for x in xs:
            if (x, y) in sol_index:
                #res += ' & ' + str(sol_index[(x, y)][entrykey])
                z = sol_index[(x, y)][entrykey]
                if isinstance(z, complex):
                    if z.imag < 1E-14:
                        z = z.real
                if isinstance(z, complex):
                    q = ' & $'
                    q += writer.write_any(z.real, entrykey, p1, p2)
                    q += ' + '
                    q += writer.write_any(z.imag, entrykey, p1, p2)
                    q += 'i$'
                    res += q
                    1
                else:
                    if abs(z) < 1E-8:
                        z = 0
                    res += ' & ' + writer.write_any(z, entrykey, p1, p2)
        res += '\\\\\n'

    res += '\\hline' + '\n'
    res += '\\end{tabu}\n'

    return res
    
    xs = sorted(xs)
    ys = sorted(ys)
    ys.reverse()
    res = ''
    corner_title = 'N = ' + str(filt['N'])
    
    res += make_text_table_header(xs, xkey, corner_title)

    for y in ys:
        q = ykey
        #if q in key_map: q = key_map[q]
        res += q + ' = ' + str(y)
        #res += q
        for x in xs:
            if (x, y) in sol_index:
                z = sol_index[(x,y)][entrykey]
                if ykey in key_map: z = val_map[ykey](z)
                res += ',' + str(z)
        res += '\n'

    return res
 
