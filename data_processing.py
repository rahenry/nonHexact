import numpy, scipy, math, cmath, random, scipy.sparse, scipy.sparse.linalg, scipy.special, sys, struct, os
import matplotlib.pyplot as plt
import solver, writer

ENCODE_EVECS = False

GLOBAL_OPTIONS = ['output_eigenvectors', 'output_precision', 'output_scheme', 'calc_correlations', 'use_saved_data', 'n_solutions', 'file_name', 'sorting_scheme', 'analysis', 'time_limit']
global_options = {}

def is_iterable(x):
    return hasattr(x, '__iter__')

def read_input_file(input_file):
    f = open(input_file)
    res = {}
    for l in f.readlines():
        l = l.split()
        if len(l) == 0: continue
        name = l[0]
        #if name[-1] == 's': name = name[0:-1]
        res[name] = []
        for x in l[1:]:
            if x == '=': continue
            if ':' in x:
                y = x.split(':')
                res[name] += range(int(y[0]), int(y[1])+1)
            else:
                try: 
                    res[name].append(int(x))
                    continue
                except ValueError: pass
                try: 
                    res[name].append(float(x))
                    continue
                except ValueError: res[name].append(x)
    return res


def encode_string(s):
    res = ''
    res += struct.pack('q', len(s))
    res += s
    return res

def encode_data(sol, key):
    if '' in key: return ''
    res = ''
    val = sol[key]
    if not is_iterable(val):
        val = [val]
    for v in val:
        if isinstance(v, complex):
            return ''
    res += encode_string(key)
    #res += struct.pack('q', len(key))
    #res += key
    if isinstance(val[0], str): 
        res += '2'
        res += encode_string(val[0])
        return res
    if isinstance(val[0], int): res += '0'
    else: res+= '1'

    dat = ''
    for x in val:
        if isinstance(x, int):
            dat += struct.pack('q', x)
        else:
            dat += struct.pack('d', x)
    res += struct.pack('q', len(dat))
    res += dat
    return res

def recode_complex(sol, key):
    val = sol[key]
    if not is_iterable(val):
        val = [val]
    res = {key + '_real': [],
            key + '_imag' : [],
            }
    for v in val:
        res[key+'_real'].append(v.real)
        res[key+'_imag'].append(v.imag)
    return res

def encode_solution(sol):
    res = ''

    new_data = {}
    for key, val in sol.iteritems():
        if not is_iterable(val):
            val = [val]
        is_complex = False
        for v in val:
            if isinstance(v, complex):
                is_complex = True
        if is_complex: new_data.update(recode_complex(sol, key))
    sol.update(new_data)
    for key in sol:
        res += encode_data(sol, key)
    res = struct.pack('q', len(res)) + res
    return res

def decode_solutions(data_file):
    if not os.path.isfile(data_file): return {}
    f = open(data_file)
    d = f.read()
    index = 0
    res = {}
    while index < len(d):
        if len(d[index:]) < 5: break
        size = struct.unpack('q', d[index:index+8])[0]
        new_sol = decode_solution(d[index:index+8+size])
        index += 8+size
        res[new_sol['ident']] = new_sol
    return res

def decode_solution(d):
    res = {}
    index = 8
    while index < len(d):
        if len(d[index:]) < 5: break
        subdata = d[index:]

        name_len = struct.unpack('q', subdata[0:8])[0]
        name = struct.unpack(str(name_len)+'s', subdata[8:8+name_len])[0]

        flag = d[index+8+name_len]
        if flag == '0': dtype = 'q'
        elif flag == '1': dtype = 'd'
        elif flag == '2': dtype = 's'


        if dtype == 's':
            data_len = struct.unpack('q', subdata[9+name_len:9+name_len+8])[0]
            data = subdata[17+name_len:17+name_len+data_len]
            res[name] = data

        else:
            data_len = struct.unpack('q', subdata[9+name_len:17+name_len])[0]
            n_data = data_len / 8
            res[name] = struct.unpack(dtype * n_data, subdata[17+name_len:17+name_len+data_len])

        if n_data == 1 and not dtype == 's':
            res[name] = res[name][0]

        index += 17+name_len+data_len

    new_data = {}
    for key in res:
        if '_real' in key:
            basekey = key.replace('_real', '')
            if is_iterable(res[key]):
                new_data[basekey] = []
                for (x, y) in zip(res[key], res[basekey+'_imag']):
                    new_data[basekey].append(x + y*1.J)
            else:
                new_data[basekey] = res[key] + res[basekey+'_imag']*1.J
    res.update(new_data)

    return res

def process_combinations(old, new):
    res = []
    for x in old:
        for y in new[1]:
            name = new[0]
            if name[-1] == 's': name = name[0:-1]
            res.append(dict(x))
            res[-1][name] = y
    return res

def get_globals(x):
    res1 = x
    res2 = {}
    for a, b in x.iteritems():
        if a in GLOBAL_OPTIONS:
            if len(b) == 1: b = b[0]
            res2[a] = b
    for a in res2:
        del res1[a]
    return (res1, res2)

def read_inputs():
    defaults = read_input_file('defaults')
    global global_options
    (defaults, global_options) = get_globals(defaults)
    global_options['file_name'] = sys.argv[1]
    input_files = sys.argv[1:]
    sols = {}
    for input_file in input_files:
        params = dict(defaults)
        (new_params, new_params_global) = get_globals(read_input_file(input_file))
        params.update(new_params)
        global_options.update(new_params_global)
        Ns = None
        Lmax = None
        global_options['params'] = dict(params)
        if 'Lmax' in params:
            Lmax = params['Lmax']
            del params['Lmax']
            Ns = params['N']
            del params['N']
            del params['L']
        sols[input_file] = {}
        combs = [[]]
        for key, val in params.iteritems():
            combs = process_combinations(combs, (key, val))

        if Lmax:
            combs_new = []
            for (Lm, N) in zip(Lmax, Ns):
                for L in range(3, Lm + 1):
                    for c in combs:
                        c_new = dict(c)
                        c_new['L'] = L
                        c_new['N'] = N
                        combs_new.append(c_new)
            combs = combs_new

        for c in combs:
            ident = writer.make_ident(c)
            sols[input_file][ident] = c
            sols[input_file][ident]['ident'] = ident
            c['fname'] = writer.make_ident_name(c)

        data_file = input_file + '.data'
        if global_options['use_saved_data']:
            stored_sols = decode_solutions(data_file)
            for ident, sol in stored_sols.iteritems():
                if ident in sols[input_file]: sols[input_file][ident] = sol
        #sols[input_file].update(decode_solutions(data_file))

        count = 0
        for ident, s in sols[input_file].iteritems():
            count += 1
            if 'e' not in s: 
                #sys.stdout.write('Solving ' + s['fname'] + '... ')
                progress = "{:5.3f}".format(count / float(len(sols[input_file])))
                print 'Solving ' + s['fname'] + '... ' + progress
                #sys.stdout.flush()
                solver.solve(s)
            s['e_infinity'] = solver.exact_eigenvalue(s)

        def zzz():
            for ident, s in sols[input_file].iteritems():
                #if not s['run_time']:
                if not s['has_solution']:
                    print 'Deleting', s['ident']
                    #if 'e' in s: print s['e']
                    del sols[input_file][ident]
                    return True
            return False
            
        if False:
            for ident, s in sols[input_file].iteritems():
                print ident, s['tol']
            while (True):
                if not zzz(): break
            print '...'
            for ident, s in sols[input_file].iteritems():
                print ident, s['tol']

            #enc = encode_solution(s)
            #dec = decode_solution(enc)

        f = open(data_file, 'w+')
        for ident, s in sols[input_file].iteritems():
            f.write(encode_solution(s))
        f.close()

        if 'sorting_scheme' not in global_options:
            global_options['sorting_scheme'] = list(global_options['output_scheme'])
            global_options['sorting_scheme'].reverse()
    return sols




